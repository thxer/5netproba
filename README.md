1, Az app/config/parameters.yml-ben tal�lhat�ak az adatb�zis be�ll�t�sai, azt be kell �ll�tani.
2, A "php app/console doctrine:database:create" paranccsal l�tre kell hozni az adatb�zist, majd a "php app/console doctrine:schema:create"-el a t�bl�kat
3, A webszerver configj�ban a DocumentRoot-ot �rdemes az alkalmaz�s web k�nyvt�r�ra �ll�tani, �gy a http://szerver.cime/-alatt lesz el�rhet� az oldal

! A json f�jl maxim�lis m�rete nem volt megadva, amennyiben nagyobb mint a be�ll�tott upload_max_filesize vagy post_max_size, a felt�lt�s nem fog siker�lni, amennyiben a m�rete nagyobb mint a memory_limit, a feldolgoz�s nem fog siker�lni, ebben az esetben fopen, fgets f�ggv�nyeket kellett volna haszn�lni �s az �llom�nyt soronk�nt feldolgozni.

a test.json nev� f�jlal teszteltem.