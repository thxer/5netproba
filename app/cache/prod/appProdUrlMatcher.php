<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        // thxer_proba_index
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'thxer_proba_index');
            }

            return array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::indexAction',  '_route' => 'thxer_proba_index',);
        }

        if (0 === strpos($pathinfo, '/bejelentkezes')) {
            // thxer_proba_login
            if ($pathinfo === '/bejelentkezes') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_thxer_proba_login;
                }

                return array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::loginAction',  '_route' => 'thxer_proba_login',);
            }
            not_thxer_proba_login:

            // thxer_proba_login_post
            if ($pathinfo === '/bejelentkezes_check') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_thxer_proba_login_post;
                }

                return array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::loginPostAction',  '_route' => 'thxer_proba_login_post',);
            }
            not_thxer_proba_login_post:

        }

        // thxer_proba_logout
        if ($pathinfo === '/kijelentkezes') {
            return array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::logoutAction',  '_route' => 'thxer_proba_logout',);
        }

        // thxer_proba_vedett
        if ($pathinfo === '/vedett') {
            return array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::vedettAction',  '_route' => 'thxer_proba_vedett',);
        }

        if (0 === strpos($pathinfo, '/json')) {
            // thxer_proba_json
            if ($pathinfo === '/json') {
                return array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::jsonAction',  '_route' => 'thxer_proba_json',);
            }

            // thxer_proba_json_upload
            if ($pathinfo === '/json_upload') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_thxer_proba_json_upload;
                }

                return array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::jsonUploadAction',  '_route' => 'thxer_proba_json_upload',);
            }
            not_thxer_proba_json_upload:

        }

        if (0 === strpos($pathinfo, '/felhasznalo')) {
            // felhasznalo
            if (rtrim($pathinfo, '/') === '/felhasznalo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'felhasznalo');
                }

                return array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::indexAction',  '_route' => 'felhasznalo',);
            }

            // felhasznalo_show
            if (preg_match('#^/felhasznalo/(?P<id>[^/]++)/mutat$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'felhasznalo_show')), array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::showAction',));
            }

            // felhasznalo_new
            if ($pathinfo === '/felhasznalo/uj') {
                return array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::newAction',  '_route' => 'felhasznalo_new',);
            }

            // felhasznalo_create
            if ($pathinfo === '/felhasznalo/keszit') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_felhasznalo_create;
                }

                return array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::createAction',  '_route' => 'felhasznalo_create',);
            }
            not_felhasznalo_create:

            // felhasznalo_edit
            if (preg_match('#^/felhasznalo/(?P<id>[^/]++)/szerkeszt$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'felhasznalo_edit')), array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::editAction',));
            }

            // felhasznalo_update
            if (preg_match('#^/felhasznalo/(?P<id>[^/]++)/frissit$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'PUT'))) {
                    $allow = array_merge($allow, array('POST', 'PUT'));
                    goto not_felhasznalo_update;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'felhasznalo_update')), array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::updateAction',));
            }
            not_felhasznalo_update:

            // felhasznalo_delete
            if (preg_match('#^/felhasznalo/(?P<id>[^/]++)/torol$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('POST', 'DELETE'))) {
                    $allow = array_merge($allow, array('POST', 'DELETE'));
                    goto not_felhasznalo_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'felhasznalo_delete')), array (  '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::deleteAction',));
            }
            not_felhasznalo_delete:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
