<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appProdUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    static private $declaredRoutes = array(
        'thxer_proba_index' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/',    ),  ),  4 =>   array (  ),),
        'thxer_proba_login' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::loginAction',  ),  2 =>   array (    '_method' => 'GET',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/bejelentkezes',    ),  ),  4 =>   array (  ),),
        'thxer_proba_login_post' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::loginPostAction',  ),  2 =>   array (    '_method' => 'POST',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/bejelentkezes_check',    ),  ),  4 =>   array (  ),),
        'thxer_proba_logout' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::logoutAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/kijelentkezes',    ),  ),  4 =>   array (  ),),
        'thxer_proba_vedett' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::vedettAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/vedett',    ),  ),  4 =>   array (  ),),
        'thxer_proba_json' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::jsonAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/json',    ),  ),  4 =>   array (  ),),
        'thxer_proba_json_upload' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\PageController::jsonUploadAction',  ),  2 =>   array (    '_method' => 'POST',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/json_upload',    ),  ),  4 =>   array (  ),),
        'felhasznalo' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/felhasznalo/',    ),  ),  4 =>   array (  ),),
        'felhasznalo_show' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::showAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/mutat',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    2 =>     array (      0 => 'text',      1 => '/felhasznalo',    ),  ),  4 =>   array (  ),),
        'felhasznalo_new' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::newAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/felhasznalo/uj',    ),  ),  4 =>   array (  ),),
        'felhasznalo_create' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::createAction',  ),  2 =>   array (    '_method' => 'post',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/felhasznalo/keszit',    ),  ),  4 =>   array (  ),),
        'felhasznalo_edit' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::editAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/szerkeszt',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    2 =>     array (      0 => 'text',      1 => '/felhasznalo',    ),  ),  4 =>   array (  ),),
        'felhasznalo_update' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::updateAction',  ),  2 =>   array (    '_method' => 'post|put',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/frissit',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    2 =>     array (      0 => 'text',      1 => '/felhasznalo',    ),  ),  4 =>   array (  ),),
        'felhasznalo_delete' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'thXer\\ProbaBundle\\Controller\\FelhasznaloController::deleteAction',  ),  2 =>   array (    '_method' => 'post|delete',  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/torol',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    2 =>     array (      0 => 'text',      1 => '/felhasznalo',    ),  ),  4 =>   array (  ),),
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens);
    }
}
