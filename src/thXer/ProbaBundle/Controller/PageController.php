<?php

namespace thXer\ProbaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use thXer\ProbaBundle\Form\FelhasznaloType;
use thXer\ProbaBundle\Entity\Felhasznalo;
use Symfony\Component\Security\Core\SecurityContext;
/**
 * Description of PageController
 *
 * @author thXer
 */
class PageController extends Controller {

    public function indexAction() {
        return $this->render("thXerProbaBundle:Page:index.html.twig");
    }
    public function loginAction() {        
        $request = $this->getRequest();
        $session = $request->getSession();
        
        $form = $this->createForm(new FelhasznaloType());
        if ($this->get('request')->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $this->get('request')->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $this->get('request')->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        return $this->render("thXerProbaBundle:Page:login.html.twig",array("form"=>$form->createView(),"error"=>$error));
    }
    public function loginPostAction(){
        return $this->redirect($this->generateUrl("thxer_proba_login"));
    }
    public function logoutAction(){        
    }
    public function vedettAction(){
        return $this->render("thXerProbaBundle:Page:vedett.html.twig");
    }
    public function jsonAction(){        
        return $this->render("thXerProbaBundle:Page:json.html.twig");
    }
    public function jsonUploadAction(){
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('thXerProbaBundle:Felhasznalo')->findAll();
        for ($i = 0; $i<count($users); $i++){
            $em->remove($users[$i]);
        }
        $em->flush();
        
        if (isset($_FILES["file"])){
            $jsonfile = file_get_contents($_FILES["file"]["tmp_name"]);
            $jdec = json_decode($jsonfile,true);
            for ($i = 0; $i<count($jdec["users"]); $i++){
                $user = new Felhasznalo();
                $user->setUsername($jdec["users"][$i]["username"]);
                $user->setName($jdec["users"][$i]["name"]);
                $user->setPassword($jdec["users"][$i]["password"]);
                $user->setEmail($jdec["users"][$i]["email"]);
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
            }
            $this->get('session')->getFlashBag()->add('notice', count($jdec["users"])." felhasználó lett felvéve!");
        }
        return $this->redirect($this->generateUrl('thxer_proba_json'));
    }
}