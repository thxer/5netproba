<?php

namespace thXer\ProbaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use thXer\ProbaBundle\Entity\Felhasznalo;
use thXer\ProbaBundle\Form\FelhasznaloType;

/**
 * Felhasznalo controller.
 *
 */
class FelhasznaloController extends Controller
{
    /**
     * Lists all Felhasznalo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('thXerProbaBundle:Felhasznalo')->findAll();

        return $this->render('thXerProbaBundle:Felhasznalo:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Felhasznalo entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Felhasznalo();
        $form = $this->createForm(new FelhasznaloType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('felhasznalo_show', array('id' => $entity->getId())));
        }

        return $this->render('thXerProbaBundle:Felhasznalo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Felhasznalo entity.
     *
     */
    public function newAction()
    {
        $entity = new Felhasznalo();
        $form   = $this->createForm(new FelhasznaloType(), $entity);

        return $this->render('thXerProbaBundle:Felhasznalo:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Felhasznalo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('thXerProbaBundle:Felhasznalo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Felhasznalo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('thXerProbaBundle:Felhasznalo:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Felhasznalo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('thXerProbaBundle:Felhasznalo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Felhasznalo entity.');
        }

        $editForm = $this->createForm(new FelhasznaloType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('thXerProbaBundle:Felhasznalo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Felhasznalo entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('thXerProbaBundle:Felhasznalo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Felhasznalo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new FelhasznaloType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('felhasznalo_edit', array('id' => $id)));
        }

        return $this->render('thXerProbaBundle:Felhasznalo:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Felhasznalo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('thXerProbaBundle:Felhasznalo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Felhasznalo entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('felhasznalo'));
    }

    /**
     * Creates a form to delete a Felhasznalo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
