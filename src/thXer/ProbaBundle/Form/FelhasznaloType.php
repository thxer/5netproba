<?php

namespace thXer\ProbaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FelhasznaloType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null, array("label"=>"Név"))
            ->add('email','email',array("label"=>"Email cím"))
            ->add('username',null, array("label"=>"Felhasználónév"))
            ->add('password','password',array("label"=>"Jelszó"))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'thXer\ProbaBundle\Entity\Felhasznalo'
        ));
    }

    public function getName()
    {
        return 'thxer_probabundle_felhasznalotype';
    }
}
